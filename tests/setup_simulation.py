#!/usr/bin/env python

import os
import time
import sys
from jinja2 import FileSystemLoader, Environment
from virl import Virl

# These have sensible defaults if they are not supplied
SIMULATION_NAME = os.environ.get('SIMULATION_NAME', 'test_simulation')
VIRL_SIMULATION_FILE = os.environ.get('VIRL_SIMULATION_FILE', './tests/simulation.virl')
DEVICE_USERNAME = os.environ.get('DEVICE_USERNAME', 'admin')
DEVICE_PASSWORD = os.environ.get('DEVICE_PASSWORD', 'admin')

# Crash if these aren't supplied
VIRL_HOST = os.environ['VIRL_HOST'] 

TIMEOUT=20*60
WAIT_TIME = 10


if __name__ == '__main__':
    virl = Virl(VIRL_HOST)
    
    if len(sys.argv) > 1 and sys.argv[1] == 'shutdown':
        #Shutdown the simulation on VIRL
        if not virl.stop_simulation(SIMULATION_NAME):
            sys.exit(1)
        sys.exit(0)
    
    # Check if VIRL simulation is running
    if not virl.simulation_running(SIMULATION_NAME):
        virl.simulation_start(SIMULATION_NAME, VIRL_SIMULATION_FILE)
    
    print("Simulation is started. \n\n")
    start_time = time.time()
    while True:
        if (time.time() - start_time) > TIMEOUT:
            sys.exit(1)
            
        ur = virl.simulation_unreachable_nodes(SIMULATION_NAME)
        if not ur:
            print("All nodes reachable")
            break
        
        print("Simulation not reachable, waiting %d seconds ... " % WAIT_TIME)
        print("Unreachable nodes: %s" % ur)
        time.sleep(WAIT_TIME)
        
    print("Simulation running and reachable.\n\n")
    print("Generating Ansible inventory")
    j2 = Environment(loader=FileSystemLoader('./'))
    ansible_template = j2.get_template('inventory.j2')
    rendered = ansible_template.render(
        nodes=virl.simulation_details(SIMULATION_NAME),
        device_username = DEVICE_USERNAME,
        device_password = DEVICE_PASSWORD
    )
    
    inventory_filename = "gen/inventory_%s" % SIMULATION_NAME    
    with open(inventory_filename, 'w') as f:
        f.write(rendered)
        
    
